# Enterprise/Channel SA collaboration process

## Goal
This project is meant to model and discuss Channel/Enterprise SA collaboration model. It is based on first successful examples of such collaboration in EMEA. The intention is to publish final process in the handbook. The reason to work on it here is that we need team collaboration and handbook process is too heavy for that.

## Scope

This process relates to Partner Sourced business only

## Process

```mermaid
graph TD;
  subgraph Discovery
    A[Partner brings a lead]-->B{{Source?}};
    B-->|GSI|C[No deal registration for GSI yet. Channel SA qualifies the opportunity and brings it to the SAL.];
    B-->|Regional|D[Opportunity in SFDC pending approval];
    C-->E{{SAL agrees to create oppotunity?}}
    E-->|yes|F[SFDC partner sourced opportunity created and approved]
    E-->|not enough information|G[Joint discovery: Channel SA, Enterprise SA, SAL, Partner, Customer]
    G-->F
    D-->H{{SAL accepts?}}
    H-->|yes|F
    H-->|no|I[Channel SA qualification with the partner]
    I-->F
  end
  subgraph Partner Enablement Stream
    J[Partner Enablement]
  end
  subgraph Prospect Nurturing Stream
    L[Prospect Nurturing]
  end
  subgraph Collaboration
    N(Frequent sync)---R(Partner slack channel)
    N---S(Weekly calls)
    O(Enable tech eval) 
    P(Verify success criteria on both ends)
    Q(Collaborate on business case)
  end
  subgraph Deal Closure
    M[Deal closure]
  end
  F-->L
  F-->J
  J-.-N
  J-.-O
  J-.-P
  J-.-Q
  L-.-N
  L-.-O
  L-.-P
  L-.-Q
  J-->M
  L-->M
```
